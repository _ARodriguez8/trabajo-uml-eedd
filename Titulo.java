import java.time.LocalDate;

public class Titulo {
	private String nombreCampeon;
	private String fechaConsecucion;

	public Titulo(String nombreCampeon,String fechaConsecucion){
		this.nombreCampeon=nombreCampeon;
		this.fechaConsecucion=fechaConsecucion;
	}
	public String getNombreCampeon() {
		return nombreCampeon;
	}
	public void setNombreCampeon(String nombreCampeon) {
		this.nombreCampeon = nombreCampeon;
	}
	public String getFechaConsecucion() {
		return fechaConsecucion;
	}
	public void setFechaConsecucion(String fechaConsecucion) {
		this.fechaConsecucion = fechaConsecucion;
	}
	

}
