import java.util.ArrayList;

public class Jugador {
	private String nombre;
	private String apellido;
	private String posicion;
	private String altura;
	private int peso;
	private String equipo;
	private ArrayList<Patrocinador> patrocinadores=new ArrayList<Patrocinador>();

	
	public Jugador(String nombre, String apellido, String posicion, String altura, int peso, String equipo) {
		this.nombre=nombre;
		this.apellido=apellido;
		this.posicion=posicion;
		this.altura=altura;
		this.peso=peso;
		this.equipo=equipo;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getPosicion() {
		return posicion;
	}


	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}


	public String getAltura() {
		return altura;
	}


	public void setAltura(String altura) {
		this.altura = altura;
	}


	public int getPeso() {
		return peso;
	}


	public void setPeso(int peso) {
		this.peso = peso;
	}


	public String getEquipo() {
		return equipo;
	}


	public void setEquipo(String equipo) {
		this.equipo = equipo;
	}


	public ArrayList<Patrocinador> getPatrocinadores() {
		return this.patrocinadores;
	}
	public void addPatrocinador(String nombre, int duracionContrato,int cantidadAportada){
		Patrocinador p=new Patrocinador(nombre, duracionContrato,cantidadAportada);
		this.patrocinadores.add(p);
	}
	public void eliminarPatrocinador(Patrocinador p){
		if(this.patrocinadores.contains(p)){
			this.patrocinadores.remove(p);
		}else{
			System.out.println("El patrocinador no tiene contrato en vigor con este jugador");
		}
	}
}