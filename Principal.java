public class Principal {
    public static void main(String[] args) {
            Equipo Boston= new Equipo("Boston Celtics", "CEL", "Este","Pacifico");
            Equipo Lakers= new Equipo("Los Angeles Lakers", "LAL", "Oeste","Atlantico");
            Jugador j1= new Jugador("Lebron", "James", "Escolta", "1.98", 100, "Lakers");
            Jugador j2= new Jugador("Jason", "Tatum", "Escolta", "2.01", 96, "Celtics");
            //añadimos a Lebron a los lakers
            Lakers.addJugador("Lebron", "James", "Escolta", "1.98", 100, "Lakers");
            //Lebron firma patrocinio con Joma
            j1.addPatrocinador("Joma", 1, 3000);
            //Los Celtics ganan el anillo de la NBA
            Boston.addTitulo("NBA Playoffs", "2018-02-27");
            
    }
}
