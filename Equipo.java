import java.util.ArrayList;
import java.time.LocalDate;

public class Equipo {
	private String nombre;
	private String abreviatura;
	private String conferencia;
	private String division;
	private ArrayList<Titulo> palmares=new ArrayList<Titulo>();
	private ArrayList<Jugador> jugadores=new ArrayList<Jugador>();
	private ArrayList<Camiseta> equipaciones=new ArrayList<Camiseta>();

	public Equipo(String nombre, String abreviatura, String conferencia,String division){
		this.nombre=nombre;
		this.abreviatura=abreviatura;
		this.conferencia=conferencia;
		this.division=division;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getAbreviatura() {
		return abreviatura;
	}
	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}
	public String getConferencia() {
		return conferencia;
	}
	public void setConferencia(String conferencia) {
		this.conferencia = conferencia;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public ArrayList<Titulo> getPalmares(){
		return this.palmares;
	}
	public ArrayList<Jugador> getJugadores() {
		return this.jugadores;
	}
	public ArrayList<Camiseta> getEquipaciones() {
		return this.equipaciones;
	}
	public void addTitulo(String nombreCampeon,String fechaConsecucion){
		Titulo t=new Titulo(nombreCampeon, fechaConsecucion);
		this.palmares.add(t);
	}
	public void addJugador(String nombre,String apellido, String posicion,String altura,int peso,String equipo){
		Jugador j=new Jugador(nombre,apellido,posicion,altura,peso,equipo);
		this.jugadores.add(j);
	}
	public void addCamiseta(String colorPrincipal,String colorSecundario,String usoCamiseta,LocalDate anoLanzamiento){
		Camiseta c=new Camiseta(colorPrincipal, colorSecundario,usoCamiseta,anoLanzamiento);
		this.equipaciones.add(c);
	}
	public void eliminarJugador(Jugador j){
		if(this.jugadores.contains(j)){
			this.jugadores.remove(j);
		}else{
			System.out.println("El jugador no pertenece a dicho equipo");
		}
	}
	public void eliminarCamiseta(Camiseta c){
		if(this.equipaciones.contains(c)){
			this.equipaciones.remove(c);
		}else{
			System.out.println("La equipacion no pertenece a dicho equipo");
		}
	}
	
}