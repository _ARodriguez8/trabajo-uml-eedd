import java.time.LocalDate;

public class Camiseta {
	private String colorPrincipal;
	private String colorSecundario;
	private String usoCamiseta;
	private LocalDate anoLanzamiento;

	public Camiseta( String colorPrincipal,String colorSecundario,String usoCamiseta,LocalDate anoLanzamiento){
		this.colorPrincipal=colorPrincipal;
		this.colorSecundario=colorSecundario;
		this.usoCamiseta=usoCamiseta;
		this.anoLanzamiento=anoLanzamiento;
	}
	public String getColorPrincipal() {
		return colorPrincipal;
	}
	public void setColorPrincipal(String colorPrincipal) {
		this.colorPrincipal = colorPrincipal;
	}
	public String getColorSecundario() {
		return colorSecundario;
	}
	public void setColorSecundario(String colorSecundario) {
		this.colorSecundario = colorSecundario;
	}
	public String getUsoCamiseta() {
		return usoCamiseta;
	}
	public void setUsoCamiseta(String usoCamiseta) {
		this.usoCamiseta = usoCamiseta;
	}
	public LocalDate getAnoLanzamiento() {
		return anoLanzamiento;
	}
	public void setAnoLanzamiento(LocalDate anoLanzamiento) {
		this.anoLanzamiento = anoLanzamiento;
	}
}
