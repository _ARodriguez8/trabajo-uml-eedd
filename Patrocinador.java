public class Patrocinador {
	private String nombre;
	private int duracionContrato;
	private int cantidadAportada;

	public Patrocinador(String nombre, int duracionContrato,int cantidadAportada){
		this.nombre=nombre;
		this.duracionContrato=duracionContrato;
		this.cantidadAportada=cantidadAportada;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getDuracionContrato() {
		return duracionContrato;
	}
	public void setDuracionContrato(int duracionContrato) {
		this.duracionContrato = duracionContrato;
	}
	public int getCantidadAportada() {
		return cantidadAportada;
	}
	public void setCantidadAportada(int cantidadAportada) {
		this.cantidadAportada = cantidadAportada;
	}
}
